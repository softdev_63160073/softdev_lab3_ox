/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer, o, x;
    private int count;
    private boolean win = false;
    private boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean setRowCol(int row, int col) {
        if(isWin() || isDraw()) return false;
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if(checkWin(row, col)){
            if(this.currentPlayer == o){
                o.win();
                x.loss();
            }else{
                x.win();
                o.loss();
            }
            this.win = true;
            return true;
        }
        if(checkDraw()){
            o.draw();
            x.draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }
    
     public boolean checkDraw() {
         if(count == 8){
             return true;
         }
         return false;
     }
    
     public boolean checkWin(int row, int col) {
        if(checkColumn(col)){
            return true;
        }
        if(checkRow(row)){
            return true;            
        }
        if(checkX()){
            return true;
        }
        return false;
    }
     
     public boolean checkColumn(int col){
       for(int r = 0; r < table.length; r++){
           if(table[r][col-1] != currentPlayer.getSymbol()) return false;
       }
       return true; 
    }  
    
    public boolean checkRow(int row){
        for(int c = 0; c < table[row-1].length; c++){
           if(table[row-1][c] != currentPlayer.getSymbol()) return false;
       }
       return true; 
    }
    
    public boolean checkX(){
        if(table[0][0] == currentPlayer.getSymbol() & table[1][1] == currentPlayer.getSymbol() & table[2][2] == currentPlayer.getSymbol())
            return true;
        
        else if(table[0][2] == currentPlayer.getSymbol() & table[1][1] == currentPlayer.getSymbol() & table[2][0] == currentPlayer.getSymbol())
            return true;
        
        else
            return false;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }
}
